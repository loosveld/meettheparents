Title: Cijfers bij het onderzoek

----

Text: 

(enkele cijfers over ouderperspectief hiervoor)

Uit de jeugdwerkbevraging blijkt dat 55% (in jaarwerking en speelpleinwerking) tot bijna 70% (in vakantiewerking) van jeugdwerkbegeleiders het belangrijk vindt dat de jeugdwerking aansluit bij wat de ouders belangrijk vinden. Tegelijkertijd vindt 30% van jeugdwerkers het moeilijk om rekening te houden met de wensen van ouders.
Jeugdwerkers zelf gaven de nood aan voor meer ondersteuning. Bijna 30% wil meer te weten komen over hoe communiceren met bepaalde doelgroepen van ouders. 50% denkt dan ook dat meer kennis over (culturele) normen van ouders/kinderen in diverse gezinssituaties hen zou helpen bij de begeleiderstaken. Bijna 25% wil weten hoe ouders (actief) te betrekken bij de werking.

##(on)Gewenste inmenging van ouders?
*Is een ouder te betrokken, perfectionistisch of overdreven bezorgd? “Gaan ze wel verantwoord om met mijn kind? Stelt mijn kind het goed op de werking?*
Ouders hebben veelal het beste voor met hun kind. Ze verwachten daarom ook van anderen dat ze hun kind alle mogelijke kansen bieden. Aanklampend gedrag van ouders kan bemoeizuchtig overkomen, maar is vaak goedbedoeld vanuit de zorg voor het kind.
- Meer dan de helft van de bevraagde jeugdwerk-begeleiders vindt dat ouders informatie moeten krijgen over welke keuzes de jeugdwerking maakt
- Meer dan de helft wil rekening houden met wensen van de ouders. 
Tegelijkertijd vindt 30% van de jeugdwerkers dat moeilijk.
- 2/3 van de bevraagde jeugdwerkbegeleiders komt regelmatig in aanraking met een ouder die ‘erg veel betrokkenheid toont’. 30% van de begeleiders ervaart dat als iets heel negatief. 
- 70% heeft het er erg moeilijk mee als ouders vertellen dat ze ergens niet akkoord mee zijn. 
- Bijna 3 op 10 jeugdwerkers kreeg al eens de vraag van een ouder om de werking aan te passen aan zijn visie of wensen. Bij ongeveer 1 op 10 zelfs meermaals.  40% van de begeleiders ervaart dat erg negatief.

*Ouders bleven in het begin van een Chirozondag vaak hangen, om de openingsspelletjes te zien, of om hun kind te zien spelen. We merkten dat de kinderen daardoor op hun ouders gefocust bleven en net heimwee kregen. En begeleiders voelden extra druk. We vragen onze ouders nu om vroeger te vertrekken, zodat de kinderen sneller op hun gemak zijn en er minder heimwee is. De deur gaat enkel open om 14:00. We nemen ook afscheid met een echt ritueel. Ludiek voor de ouders en het werkt! “ (Een jeugdleidster)
“We gingen dit jaar op zoek naar een nieuw leidingslokaal en deden een post via Facebook. Ouders willen helpen, en dat is nuttig, maar we moeten een goed evenwicht vinden. Veel ouders hebben heel veel expertise en connecties. Er komen dan soms ideeën die voor ons niet altijd werkbaar zijn. We hebben gevraagd de ouders in een privébericht alle ideeën en informatie te bezorgen zodat we ook persoonlijk kunnen reageren.(Een jeugdleidster)*

##Negatieve reacties van ouders?

Net zoals bij professionals of beroepskrachten kan het bij jeugdwerkbegeleiders al eens misgaan, daar leren ze ook uit. En een begeleider gaat om met 'het hoogste goed' van ouders, namelijk hun kind. Is het dan niet meer dan normaal dat ouders wel eens wikken en wegen of ze jeugdwerkers kunnen vertrouwen? Klagende of boze ouders kunnen echter vijandig en beschuldigend overkomen. Boosheid heeft soms (terecht) te maken met ontevredenheid. Vaker is het een uiting van grote bezorgdheid over hun kind. Het kan vervelend zijn als je de klachten via via hoort. Als ouders zich niet gehoord voelen, zoeken ze het soms hoger op, uit onvrede of omdat ze niet meteen weten waar ze terecht kunnen met een klacht.

- 2/3 van de bevraagde jeugdwerkbegeleiders vindt het belangrijk om de jeugdwerking aan te passen bij klachten of opmerkingen van ouders
- 1/3 vindt dit moeilijk om aan te pakken
- 18% wil hier ondersteuning en vorming over krijgen.
- 17% van de jeugdwerkers krijgt vaak te maken met een ouder die klaagt bij een derde partij over de werking 61% vindt dit erg negatief.
- een kleine 5% van de bevraagde jeugdwerkers wordt al eens geconfronteerd met ouders die met een klacht naar de media stappen. 1/3 van de bevraagde jeugdwerkers ervaart dit als zeer negatief. Ze geven eveneens aan meer aandacht hieraan te willen besteden in vormingen.

*Als ouder ga je zelf soms pas communiceren als er iets ergs gebeurt. Ik vind dat zelf heel vervelend want dat is soms erg overtrokken. Leiders doen zaterdag na zaterdag hun best om een goed programma samen te stellen. Ik betrap me erop dat ik niet steeds een dankjewel zeg. Als het iets gevaarlijk is, bv. in de mais spelen tijdens het oogstseizoen, wil je wel op de gevaren wijzen. Niet alle leiding staat er bij stil. Op zich lijk je dan heel negatief, maar dat is ook maar een momentopname dan. Als ouder probeer ik wel de gelegenheid te nemen ook positieve prikkels te geven bv. door aanwezig te zijn op spaghetti-avonden waar je de gelegenheid krijgt al eens een schouderklopje te geven of gewoon een informatief babbeltje te slaan. (Een ouder)*
 
##Goede afspraken, te veel of te weinig communicatie? 
Dat ouders niet meewerken, de afspraken niet nakomen, zich niet als eerste opgeven om mee te werken aan het eetfestijn, … heeft niet altijd met desinteresse of onwil te maken. Voor sommige ouders komt het er gewoon niet van, anderen vermijden misschien contact uit angst voor een negatieve boodschap.

Wanneer jeugdbegeleiders te maken krijgen met gescheiden ouders, kan dat een extra uitdraging betekenen. Simpele afspraken zoals wie komt het kind brengen of halen, of kan het kind alleen thuis geraken (en naar welk adres moet het dan gaan), worden opeens een stuk ingewikkelder. Voor sommige ouders is het niet evident om informatie over de werking of organisatie te vinden, bv. omdat ze niet weten waar te zoeken, iets waar jeugdwerkbegeleiders zich zelf vaak ook in kunnen herkennen.

- bij meer dan 10% van de bevraagde jeugdwerkbegeleiders komt het regelmatig voor dat ouders het oneens zijn met elkaar en ieder andere afspraken vraagt. 34% vindt dat erg lastig en 20% vraagt hierover meer aandacht in vorming.
- 1/3 van de jeugdwerkers geeft aan meer vorming te willen over communicatie met bepaalde doelgroepen van ouders.
- bij 16% van de jeugdwerkbegeleiders gebeurt het regelmatig dat een ouder geen of onvoldoende informatie geeft over bijzondere kenmerken (zoals allergieën, leerstoornis, mentale gezondheid...) van zijn of haar kind. 55% ervaart dat als zeer negatief. 17% wil hierover meer aandacht in vorming.

*Doordat we actief in de bossen en beken spelen komen kinderen soms met vuile kledij thuis. Voor ons is net dat Chiro. Dat krijgen we niet altijd uitgelegd, zeker niet aan nieuwe ouders.  (Een jeugdleidster)*

*Ouders moeten weten wie te contacteren in geval van nood, maar als ouders hebben we zelf ook een verantwoordelijkheid. Ook de leiding hoort juiste contactgegevens te hebben, juiste afspraken te kennen. Maar ik moet eerlijk toegeven dat ook al eens het geld van een weekend vergeet over te schrijven of tijdig te betalen. Ik ben soms wat van de slordige kant maar het heeft zeker ook wel met de manier van communiceren te maken. Soms wordt er zus gecommuniceerd via briefwisseling. In andere afdelingen doen leiders het zo. Dat maakt het niet altijd zo makkelijk het vlot op te volgen. (Een ouder)*

*De leidsters vroegen me of mijn zoontjes mee mochten op tweedaagse. Ik stemde in. Toen ik zag dat het 75 euro per kind was, hield me dat wel tegen. Het was tenslotte maar voor 2 dagen. De dag van vertrek kreeg ik telefoon ‘waar de kinderen bleven’. Ik ken de leidster die me belde goed. Zij kent mijn situatie. Ik zei haar eerlijk dat ik het toch wat duur vond. Ze lichtte toe dat dat ons helemaal niet mocht tegenhouden aangezien er ook een sociaal tarief is. Dat stond bijvoorbeeld niet in de brief. Ik heb dan halsoverkop de rugzakken gereed gemaakt. Ik ben blij dat ze de moeite deed me aan boord te trekken want mijn zoontjes hadden er een fantastische tijd. (Een ouder)*

*We hebben een Facebookgroep waar we aan meer informele groepscommunicatie doen met ouders. We kiezen bewust voor een Facebookgroep waarin ouders vrij kunnen communiceren en een pagina voor de meer officiële communicatie. De meer informele groep gebruiken we  enkel voor oproepen zoals ‘Helpende handen gezocht!’. We proberen het familiaal te houden. Zo ontstaat er solidariteit en communicatie onder de ouders. Dat schept een band. Ouders kunnen ook in de groep informatie met elkaar uitwisselen. (Een jeugdleidster) *

*Ik heb zelf nooit in de Chiro gezeten. Ik kwam als onbekende die leefwereld binnen, maar ben wel aangenaam verrast. Er waren een aantal ouders die ik al kende van op school, maar ook heel wat ouders die ik niet ken. Je hoort al eens van je kinderen verhalen van andere ouders, wie ze zijn, wat ze doen. Onze kinderen spelen heel vaak met elkaar. Ik hielp mee op een ouderbijeenkomst laatst en dat was heel tof. We konden praten oven de kinderen, school en leerden elkaar zelf als ouder ook beter kennen. (Een ouder)*

Nog link naar bestanden toevoegen