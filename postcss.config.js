module.exports = ctx => ({
  plugins: [
    require('postcss-import'),
    require('postcss-preset-env'),
    ctx.env === 'production' ? require('cssnano')({}) : false
  ]
});
