<!doctype html>
<html lang="en">

<head>

     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width,initial-scale=1.0">

     <title><?= $site->title() ?> | <?= $page->title() ?></title>

     <?= css([
    'https://fonts.googleapis.com/css?family=Ropa+Sans',
    'assets/css/style.css',
    '@auto'
    ]) ?>

</head>

<body class="<?php echo $page->slug(); ?> ">
     <aside>
          <?php if(!$page->isHomepage()): ?>
          <nav class="menu">
               <h2>Thema's</h2>
               <?php foreach ($site->find('themas')->children()->listed() as $theme): ?>
               <a href="<?php echo $theme->url() ?>"
                    class="bg-<?php echo $theme->slug() ?>-hover <?php echo e($theme->isActive(),'active','')?>"><?php echo $theme->title() ?></a>
               <?php endforeach ?>
          </nav>
          <?php else: ?>
          <div class="intro">
               <h5><?php echo $site->text()  ?></h4>
          </div>
          <?php snippet('search',['query' => '']) ?>
          <?php endif; ?>
          <div class="logo"><a href="/<?php echo $site->homepage(); ?>"><img
                         src="<?php echo url('assets/images/logo.png') ?>" alt="Meet The Parents"></a></div>

     </aside>
     <main class="h-100">