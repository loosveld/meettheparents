     </main>

     <footer class="footer">
          <div class="d-flex align-items-center"><a href="/" class="ml-5 ">Start</a> <a
                    href="<?php echo $pages->find('about')->url(); ?>" class="ml-5 ">Over ons</a></div>
          <div class="partners">
               <?php foreach ($site->files()->template('partner')->sortBy('sort', 'asc') as $partner): ?>
               <div class="partner d-flex flex-column justify-content-center ">
                    <a href="<?php echo $partner->link(); ?>" target="_blank" aria-label="<?php echo $page->title();?>"><?php echo $partner->thumb([
               'quality'    => 80,
               'width'      => 200,
               ])->html(); ?></a>
               </div>
               <?php endforeach ?>
          </div>
     </footer>



     </body>

     </html>