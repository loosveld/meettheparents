<a href="<?php echo $theme->url(); ?>" id="<?php echo $theme->slug();?>" class="theme">
     <div class="title"><?php echo $theme->title() ?></div>
     <div class="summary"><?php echo Str::substr($theme->summary(), 0, 200) ?>&nbsp;...</div>
     <img src="<?php echo $theme->illustration()->toFile()->thumb([
          'quality'    => 100,
          'width'      => 200,
        ])->url(); ?>" />
</a>