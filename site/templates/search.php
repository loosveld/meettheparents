<?php snippet('header') ?>

<div class="p-5">
     <div class="mb-4">
          <h1>Zoekresultaten</h1>
          <?php snippet('search') ?>

          <h2><b><?php echo $results->count(); ?></b> resultaten</h2>
     </div>
     <div>
          <dl>
               <?php foreach ($results as $result): ?>
               <dt>
                    <a href="<?= $result->url() ?>">
                         <?= $result->title() ?>
                    </a>
               </dt>
               <dd class="border-bottom pb-2 ">
                    <a href="<?= $result->url() ?>" style="font-size:14px; color: #aaa">
                         <?= $result->url() ?>
                    </a>
               </dd>
               <?php endforeach ?>
          </dl>
     </div>
</div>

<?php snippet('footer') ?>