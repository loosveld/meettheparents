<?php snippet('header') ?>

<div class="p-5">
     <div class="mb-4">
          <h1><?php echo $page->title(); ?></h1>

          <nav class="submenu color-white px-4 bg-<?php echo $page->slug(); ?> submenu-<?= $page->template() ?> "
               style="border-radius:.75rem;">
               <a href="#part1" class="color-beige py-2">Situatie onder de loep</a><span>>>></span>
               <a href="#part2" class="color-beige py-2">Verder inzoomen</a><span>>>></span>
               <a href="#part3" class="color-beige py-2">Situatie anders aanpakken</a><span>>>></span>
               <a href="#part4" class="color-beige py-2">OK GO!</a>
          </nav>
     </div>
     <div>
          <div class="field position-relative bg-<?php echo $page->slug(); ?>"
               style="border-top-left-radius:1rem; border-top-right-radius:1rem">
               <a name="part1"></a>
               <div class="field-content color-beige">
                    <?php foreach ($page->situations()->split() as $situation): ?>
                    <div class="mb-4 py-2 color-beige"><?php echo page($situation)->title(); ?></div>
                    <?php endforeach ?>
               </div>
               <div class="field-title bg-diagonal">
                    <div>1</div>
                    <div>situatie&nbsp;onder de loep</div>
               </div>
          </div>
          <div class="field position-relative bg-default">
               <a name="part2"></a>
               <div class="field-content h2-color-ouders-laten-meewerken">
                    <?php echo $page->text_2()->kirbyText(); ?>
               </div>
               <div class="field-title bg-diagonal-dark">
                    <div>2</div>
                    <div>verder<br>inzoomen</div>
               </div>
          </div>
          <div class="field position-relative bg-<?php echo $page->slug(); ?>">
               <a name="part3"></a>
               <div class="field-content h2-beige"><?php echo $page->text_3()->kirbyText(); ?></div>
               <div class="field-title bg-diagonal">
                    <div>3</div>
                    <div>situatie&nbsp;anders<br />aanpakken</div>
               </div>
          </div>
          <div class="field position-relative bg-default">
               <a name="part4"></a>
               <div class="field-content"><?php echo $page->text_4()->kirbyText(); ?></div>
               <div class="field-title bg-diagonal-dark">
                    <div>4</div>
                    <div>OK...&nbsp;GO!</div>
               </div>
          </div>
     </div>

</div>
<?php snippet('footer') ?>