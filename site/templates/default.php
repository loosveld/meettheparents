<?php snippet('header') ?>

<div class="p-5">
     <div class="mb-4">
          <h1><?php echo $page->title(); ?></h1>
          <nav class="submenu px-4 bg-default " style="border-radius:.75rem;">
               <?php foreach ($page->children() as $child):  ?>
               <a href="<?php echo $child->url(); ?>" class="color-beige py-2"><?php echo $child->title(); ?></a>
               <?php endforeach; ?>
          </nav>
     </div>
     <div>
          <?= $page->text()->kt() ?>
     </div>
</div>

<?php snippet('footer') ?>