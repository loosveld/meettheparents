<?php snippet('header') ?>

<div class="theme-grid h-100 px-5" style="padding-bottom: 2rem !important; padding-top: 2rem !important">
     <?php foreach ($site->find('themas')->children()->listed() as $theme): ?>
     <?php if(!$theme->illustration()->isEmpty()): ?>
     <?php snippet('theme', ['theme' => $theme]) ?>
     <?php endif; ?>
     <?php endforeach ?>
</div>

<?php snippet('footer') ?>