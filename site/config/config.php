<?php

return [
    'debug' => true,
    'panel' =>[
        'install' => true
    ],
    'hooks' => [
        'page.create:after' => function ($page) {
            $page->changeStatus('listed');
        }
    ]
];